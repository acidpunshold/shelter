<?php
namespace classes;

use classes\animals\Cat;
use classes\animals\Dog;
use classes\animals\Turtle;

/**
 * Animal Class
 */

abstract class Animal
{
    /**
     * @var string $name
     * @var int $age
     * @var string $date
     */

    private $name;
    private $age;
    private $dateCreate;


    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
        $this->dateCreate = date('d/m/Y H:i:s', time());
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return integer
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getDateCreate(): string
    {
        return $this->dateCreate;
    }
}
