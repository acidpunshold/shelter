<?php
use classes\Shelter;


use classes\animals\Cat;
use classes\animals\Dog;
use classes\animals\Turtle;

spl_autoload_register(function($name) {
    $filename = $name . '.php';
    if (stream_resolve_include_path($filename) === false) {
        return;
    }
    require_once $filename;
});

$shelter = new Shelter();

//Отдать живтное любого вида
echo "Выдача любого животного\n";
var_dump ($shelter->giveAnimal());

//Отдать животное по типу
echo "Выдача кота\n";
var_dump ($shelter->giveAnimal(Cat::class));
echo "Выдача пса\n";
var_dump ($shelter->giveAnimal(Dog::class));
echo "Выдача черепахи\n";
var_dump ($shelter->giveAnimal(Turtle::class));
echo "Выдача попугая\n";
var_dump ($shelter->giveAnimal('Parrot'));
// Добавление животных
echo "Добавление животных\n";
var_dump($shelter->addAnimal('Первый пёс', 1, Dog::class));
sleep(1);
var_dump($shelter->addAnimal('Второй пёс', 2, Dog::class));
sleep(1);
var_dump($shelter->addAnimal('Первая черепаха', 16, Turtle::class));
sleep(1);
var_dump($shelter->addAnimal('Первый кот', 43, Cat::class));
sleep(1);
var_dump($shelter->addAnimal('Второй кот', 32, Cat::class));
sleep(1);
var_dump($shelter->addAnimal('Третий пёс', 42, Dog::class));
sleep(1);
var_dump($shelter->addAnimal('Попугай', 42, 'Parrot'));
sleep(1);

//Список животных отсортированный по имени
echo "Список животных\n";
var_dump ($shelter->viewAnimals());

echo "Список псов\n";
var_dump ($shelter->viewAnimals(Dog::class));
echo "Список котов\n";
var_dump ($shelter->viewAnimals(Cat::class));
echo "Список черепах\n";
var_dump ($shelter->viewAnimals(Turtle::class));

//Отдать живтное любого вида
echo "Выдача любого животного\n";
var_dump ($shelter->giveAnimal());

//Отдать животное по типу
echo "Выдача кота\n";
var_dump ($shelter->giveAnimal(Cat::class));
echo "Выдача пса\n";
var_dump ($shelter->giveAnimal(Dog::class));
echo "Выдача черепахи\n";
var_dump ($shelter->giveAnimal(Turtle::class));
echo "Выдача черепахи\n";
var_dump ($shelter->giveAnimal(Turtle::class));
echo "Выдача попугая\n";
var_dump ($shelter->giveAnimal('Parrot'));
echo "Выдача пса\n";
var_dump ($shelter->giveAnimal(Dog::class));
