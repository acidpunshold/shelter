<?php
namespace classes;


use classes\animals\Cat;
use classes\animals\Dog;
use classes\animals\Turtle;


/**
* Shelter class
*/
class Shelter
{
    /**
    * @var array $animals
    */
    private $animals;


    public function __construct()
    {
        $this->animals = [];
    }

    /**
    * @param string $name
    * @param int $age
    * @param string $type
    *
    * @return bool
    */
    public function addAnimal(string $name, int $age, string $type): bool
    {
        if (!class_exists($type)) {
            return false;
        }
        $newAnimal = new $type($name, $age);

        $this->animals[] = $newAnimal;
        return true;
    }

    /**
    * @param string $type
    *
    * @return array
    */
    public function viewAnimals(string $type = ''): ?array
    {
        $animals = $this->getAnimalsByType($type);
        if (!empty($animals)) {
            $animals = $this->sortAnimalsByName($animals);
        }

        return $animals;
    }

    /**
    * @param string $type
    *
    * @return array
    */
    public function giveAnimal($type = ''): ?Animal
    {
        $animals = $this->getAnimalsByType($type);
        if (empty($animals)){
            return null;
        }

        $firstAnimal = null;
        $firstAnimalId = null;
        if (count($animals) > 0) {
            foreach ($animals as $id => $animal) {
                if (empty($firstAnimal)) {
                    $firstAnimal = $animal;
                    $firstAnimalId = $id;
                    continue;
                }
                if (strtotime($animal->getDateCreate()) < strtotime($firstAnimal->getDateCreate())) {
                    $firstAnimal = $animal;
                    $firstAnimalId = $id;
                }
            }
        }
        $this->removeAnimal($firstAnimalId);
        return $firstAnimal;
    }

    /**
    * @param string $type
    *
    * @return array
    */
    private function getAnimalsByType(string $type): ?array
    {
        if (empty($this->animals)){
            return null;
        }

        if (!empty($type)){
            $animals = array_filter($this->animals, function (Animal $animal) use ($type) {
                return $animal instanceof $type;
            });
        } else {
            $animals = $this->animals;
        }
        return $animals;
    }

    /**
    * @param array $animals
    *
    * @return array
    */
    private function sortAnimalsByName(array $animals): array
    {
         usort($animals, function (Animal $animal1, Animal $animal2) {
            return $animal1->getName() <=> $animal2->getName();
        });
        return $animals;
    }

    /**
    * @param int $id
    *
    * @return void
    */
    private function removeAnimal (int $id): void
    {
        unset($this->animals[$id]);
    }
}
